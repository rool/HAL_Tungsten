# Copyright 2002 Tematic Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Tungsten HAL
#

COMPONENT = Tungsten HAL
TARGET    = Tungsten
OBJS      = Top Boot IIC Interrupts Timers NVMemory MachineID PCIasm DebugPCI \
            Video PCI CLib CLibAsm ATA M1535DMA KbdScan UART Audio MSI RTC PCItung
CINCLUDES = ${TCPIPINC}

include HAL

CFLAGS   += -APCS 3/32bit/nofp/noswst
ASFLAGS  += -APCS 3/nofp/noswst

# Dynamic dependencies:
