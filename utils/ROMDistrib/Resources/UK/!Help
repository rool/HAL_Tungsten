
IYONIX pc Flash ROM Programmer
==============================

This application is responsible for re-programming the Flash ROM in the
IYONIX pc with a new version of RISC OS.

Programming the Flash ROM is done in a five part process:

1. The new ROM image is loaded in to memory and checksummed.
2. A backup copy of the former ROM contents is written to 
   the hard disc root dir, see below.
3. Any patching required is undertaken on the ROM image.
4. The Flash ROM is erased.
5. The new ROM image is programmed into the Flash ROM.

You will additionally be given the option to program either a "Full ROM"
or a "Transfer ROM". The "Transfer ROM" is for use primarily before
swapping or installing graphics cards.

Once stage four has begun it is important that the computer is
not reset until the completion of stage five. If for any reason
the computer is re-set at any point during the stages four and
five it will be left with an incomplete version of RISC OS in
the Flash ROM and may no longer start up.
In this instance the computer must be returned to Castle for
re-programming.

If in doubt, you can always run the programmer a second time before
resetting the machine. If the contents of the Flash ROM are already
up to date then the programmer will tell you so, without attempting 
any more programming.

Do NOT re-program the Flash ROM with an earlier version of
RISC OS than that with which the computer was supplied. Doing
so may result in serious loss of functionality requiring it to
be returned to Castle for re-programming.

Using the restore programmer
============================

An application, !RestoreXXX is created in the root directory of 
your harddisc (ADFS::4.$), where XXX is the previous ROM version. For
example 510 for RISC OS 5.10, make a note of your previous version from the
task switcher 'Info' box.

If after the upgrade the machine starts (denoted by the beep from the
speaker before the keyboard is active) but you are unable to see any 
output on the monitor it is possible to restore your previous ROM 
'blind'.

Wait for the computer to boot (keyboard active, harddisc inactive) then
press the F12 key.

Then type
  !Restore510 -restorerom
and press return. Leave your machine for at least 5 minutes before doing
anything else. If the restore was successful then on rebooting
you'll be back where you started.

