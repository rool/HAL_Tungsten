; Copyright 2002 Tematic Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        AREA    |Asm$$Code|, CODE, READONLY, PIC

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>

        GET     Hdr:OSEntries
        GET     Hdr:HALEntries
        GET     Hdr:VideoDevice

        GET     hdr.StaticWS
        GET     hdr.80321

        EXPORT  Video_init

VideoDevice
        DCW     HALDeviceType_Video + HALDeviceVideo_VDU
        DCW     HALDeviceID_VDU_Tungsten
        DCD     HALDeviceBus_Exp + HALDeviceExpBus_PCI
        DCD     0               ; API version 0
        DCD     VideoDevice_Desc
        DCD     0               ; Address - N/A
        %       12              ; Reserved
        DCD     VideoDevice_Activate
        DCD     VideoDevice_Deactivate
        DCD     VideoDevice_Reset
        DCD     VideoDevice_Sleep
        DCD     -1
        DCD     0
        %       8
        DCD     DefaultStraps
        ASSERT (.-VideoDevice) = HALDevice_VDU_Size

; Don't change this string or its positioning relative to DefaultStraps, or
; else the rom flash tool won't be able to find the DefaultStraps table
VideoDevice_Desc
        =       "NVidia STRAP data", 0
        ALIGN

; On Tungsten, some cards get 'confused' on startup and report wrong
;  configuration information.
; For any PCI slot occupied when the ROM is programmed we store it's
;  PCI ID and default strap value here.
; Tungsten has 4 PCI slots, so up to 4 sets of info are stored
; second word is strap info, first word (of the pair) is the
; PCI device ID of the graphics card found, or -1 if none
DefaultStraps
        DCD     -1                 ; PCI Vendor/Product ID, -1 = none
        DCD     0                  ; bus 0 slot 6 strap info
        DCD     -1
        DCD     0                  ; bus 0 slot 7
        DCD     -1
        DCD     0                  ; bus 1 slot 8
        DCD     -1
        DCD     0                  ; bus 1 slot 9

Video_init
        ; Called during HAL_InitDevices
        ; Register the device that contains the NVidia STRAP info
        ; But only if the info has been patched in (allows NVidia module to
        ; easily fall back to other methods, e.g. if old ROM programmer used)
        LDR     a1, DefaultStraps
        LDR     a2, DefaultStraps+8
        LDR     a3, DefaultStraps+16
        LDR     a4, DefaultStraps+24
        CMP     a1, #-1
        CMPEQ   a2, #-1
        CMPEQ   a3, #-1
        CMPEQ   a4, #-1
        MOVEQ   pc, lr
        MOV     a1, #0
        ADR     a2, VideoDevice
        CallOS  OS_AddDevice, tailcall

VideoDevice_Activate
VideoDevice_Deactivate
VideoDevice_Reset
        MOV     pc, lr

VideoDevice_Sleep
        MOV     a1, #0
        MOV     pc, lr

        END
